const mongoose = require('mongoose')
module.exports = function cleanUserId (id) {
  if (id.toString().includes('|')) {
    return new mongoose.mongo.ObjectId(id.split('|')[1])
  }
  return mongoose.mongo.ObjectId(id)
}

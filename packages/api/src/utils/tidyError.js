module.exports = function tidyError (error) {
  return {
    type: error.type,
    message: error.message,
    code: error.code
  }
}

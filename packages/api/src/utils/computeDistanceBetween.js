// This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
module.exports = function computeDistanceBetween (point1, point2) {
  const lat1 = point1[0]
  const lon1 = point1[1]
  const lat2 = point2[0]
  const lon2 = point2[1]
  const R = 6371.230 // km
  const dLat = toRad(lat2 - lat1)
  const dLon = toRad(lon2 - lon1)
  const lat1Rad = toRad(lat1)
  const lat2Rad = toRad(lat2)

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1Rad) * Math.cos(lat2Rad)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  return R * c
}

// Converts numeric degrees to radians
function toRad (Value) {
  return Value * Math.PI / 180
}

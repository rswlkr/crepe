/* eslint-disable camelcase */
const router = require('express').Router()
const pusher = require('./controllers/Pusher')

router.get('/alive', (req, res) => res.send('👍'))

router.route('/v1/pusher/send').post(pusher.send)
router.route('/v1/pusher/auth').post(pusher.auth)

module.exports = router

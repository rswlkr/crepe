const P = require('pusher')

const pusher = new P({
  appId: process.env.PUSHER_ID,
  key: process.env.PUSHER_KEY,
  secret: process.env.PUSHER_SECRET,
  cluster: process.env.PUSHER_CLUSTER,
  useTLS: true
})

class Pusher {
  static async auth (req, res) {
    const socketId = req.body.socket_id
    const channel = "messages"
    const auth = pusher.authenticate(socketId, channel)
    res.send(auth)
  }

  static async send (req, res) {
    const {message,name, room} = req.body
    const channel = "messages"
    pusher.trigger(channel, 'new_message', { name, message })
    res.sendStatus(200)
  }
}

module.exports = Pusher

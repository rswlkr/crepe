const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

const serverless = require('serverless-http')
const express = require('express')

const routes = require('./routes')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(cors({ origin: '*' }))

app.use(express.static(path.join(__dirname, '/public')))
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use(routes)

module.exports.handler = serverless(app, {
  request: function (req, event, context) {
    req.event = event
    req.context = context
  }
})

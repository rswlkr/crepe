import React from 'react'
import axios from 'axios'
import { useState } from 'react'
import Jitsi from 'react-jitsi'
import {useParams} from 'react-router-dom'

const Room = (props) => {
  const {id} = useParams()
  return (
    <>
      <Jitsi roomName={`crepe_${id}`} coreConfig={{enableWelcomePage: false, chromeExtensionBanner:{}}} interfaceConfig={{ filmStripOnly: true, TOOLBAR_BUTTONS:['microphone','camera'], VERTICAL_FILMSTRIP:false, DEFAULT_BACKGROUND:'#27474E',SHOW_CHROME_EXTENSION_BANNER:'false', DISPLAY_WELCOME_PAGE_CONTENT:false,DISABLE_VIDEO_BACKGROUND:true }} containerStyle={{ width: '100%', height:'100%',display:'flex'}}/>
    </>
  )
}

export default Room



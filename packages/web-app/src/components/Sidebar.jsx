import React from 'react'
import axios from 'axios'
import { useState } from 'react'

const Sidebar = (props) => {
  const [message,setMessage] = useState('');
  const [name,setName] = useState('');
  const [nameConfirmed,setNameConfirmed] = useState(false);


  const sendMessage = (message) => {
    if (message.length >0) {
      axios.post('/pusher/send',{
        name: name,
        message: message
      })
      setMessage('')
    }

  }

  const leadingZeroHours = (x) => {
    return (x.getHours() < 10 ? '0' : '') + x.getHours()
  }
  const leadingZerosMins = (x) => {
    return (x.getMinutes() < 10 ? '0' : '') + x.getMinutes()
  }
  const leadingZeroSeconds = (x) => {
    return (x.getSeconds() < 10 ? '0' : '') + x.getSeconds()
  }

  return (
    <>
      <div style={{flex: 1, display:'flex', flexDirection:'column', borderRight:'1px white solid', padding: '10px', minWidth:'0', overflowWrap: 'break-word'}}>
        <div style={{margin:'0', fontSize:'1.6rem', borderBottom:'1px solid #27474E'}}><img width='32px' src="logo.png"/> </div>
        <div style={{display:'flex', marginTop:'auto', flexDirection:'column' }}>
        <div style={{overflow: 'scroll'}}>
          {props.messages.length > 0 && props.messages.map((message) => (
            <p style={{margin:'2px'}} key={message.timestamp.getMilliseconds()}><span style={{fontSize:'12px'}}>{message.name}</span> {message.message}</p>
          ))}
        </div>
          {!nameConfirmed && (
            <div style={{padding:'5px', margin:'10px'}}>
              <input value={name} onChange={(e) => setName(e.target.value)} onKeyDown={(e) => e.key === 'Enter' ? setNameConfirmed(true) : null} placeholder='Set your name' style={{height: '32px',boxSizing : "border-box", width: '80%'}}/>
              <button onClick={() => setNameConfirmed(true)} style={{width: '20%',height:'32px', backgroundColor:'#27474E', color:'white'}}>Set</button>
            </div>
          )}
          {nameConfirmed && (
          <div style={{padding:'5px', margin:'10px'}}>
          <input value={message} onChange={(e) => setMessage(e.target.value)} onKeyDown={(e) => e.key === 'Enter' ? sendMessage(message) : null} placeholder='Send a message' style={{height: '32px',boxSizing : "border-box", width: '80%'}}/>
          <button  onClick={() => sendMessage(message)} style={{width: '20%',height:'32px', backgroundColor:'#27474E', color:'white'}}>send</button>
        </div>
          )}

        </div>
    </div>
  </>
  )
}

export default Sidebar


